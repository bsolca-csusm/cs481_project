﻿using System;
using System.IO;
using Fitlytics.iOS.Persistence;
using Fitlytics.Persistence;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(SqLiteDb))]

namespace Fitlytics.iOS.Persistence
{
    internal class SqLiteDb : ISqLiteDb
    {
        public SQLiteAsyncConnection GetConnection()
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); 
            var path = Path.Combine(documentsPath, "MySQLite.db3");

            return new SQLiteAsyncConnection(path);
        }
    }
}