﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fitlytics
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Registration : ContentPage
    {
        public Registration()
        {
            InitializeComponent();
        }

        private void NameCompleted(object sender, EventArgs e)
        {
            var app = Application.Current as App;
            app.Name = NewUserName.Text;
            app.IsUserCreated = true;
            Application.Current.MainPage = new NavigationPage(new MainPage());
            Navigation.PopToRootAsync();
        }
    }
}