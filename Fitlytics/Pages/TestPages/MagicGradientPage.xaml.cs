﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

/*
 * MagicGradients Github
 * https://github.com/mgierlasinski/MagicGradients
 *
 * GradientMagic Generator in CSS
 * https://www.gradientmagic.com/
 */
namespace Fitlytics
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MagicGradient : ContentPage
    {
        public MagicGradient()
        {
            InitializeComponent();
        }
    }
}