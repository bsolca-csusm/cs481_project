﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Timers;
using Fitlytics.Models;
using Fitlytics.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Timer = System.Timers.Timer;

namespace Fitlytics.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SessionPage : INotifyPropertyChanged
    {
        private SessionModel _session = new SessionModel();
        internal ObservableCollection<MyExerciceModel> MyExercicesObsCollection;
        private Timer _timer = new Timer();
        private bool _isNewSession;
        private StopWatchWithOffset _watch;

        public new event PropertyChangedEventHandler PropertyChanged;

        public SessionModel MySession
        {
            get => _session;
            set
            {
                _session = value;
                OnMyPropertyChanged();
            }
        }

        public StopWatchWithOffset SessionWatch
        {
            get => _watch;
            set
            {
                _watch = value;
                OnMyPropertyChanged();
            }
        }

        private void OnMyPropertyChanged([CallerMemberName] string stringProperty = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(stringProperty));
        }

        public SessionPage(Guid sessionId)
        {
            _isNewSession = false;
            MySession.SessionGuid = sessionId;

            InitializeComponent();
        }
        public SessionPage()
        {
            InitializeComponent();
            _isNewSession = true;
            MySession.SessionGuid = Guid.NewGuid();
            MySession.Start = DateTime.Now;
        }

        protected override void OnAppearing()
        {
            BindingContext = this;
            base.OnAppearing();
            SessionWatch = new StopWatchWithOffset();
            _timer.Interval = TimeSpan.FromSeconds(1).TotalMilliseconds;
            _timer.Elapsed += TimerOnElapsed;
            if (_isNewSession)
            {
                App.Db.AddSession(MySession);
                RunTimers();
                _isNewSession = false;
            }
            else
            {
                GetSession();
            }
        }

        protected override void OnDisappearing()
        {
            MySession.TimeElapsed = SessionWatch.ElapsedTimeSpan;
            App.Db.UpdateSession(MySession);
            base.OnDisappearing();
        }
        private void RunTimers()
        {
            SessionWatch.Start();
            _timer.Start();
        }
        private void StopTimers()
        {
            SessionWatch.Stop();
            _timer.Stop();
        }
        private async void GetSession()
        {
            MyExercicesObsCollection = App.Db.GetMyExercices(MySession.SessionGuid).Result;
            ExercicesList.ItemsSource = MyExercicesObsCollection;
            await Task.Run(() =>
             {
                 MySession = App.Db.GetSession(sessionGuid: MySession.SessionGuid).Result;
                 SessionWatch.SetElapsedTimeSpan(MySession.TimeElapsed);
                 TimerOnElapsed(null, null);
                 if (MySession.IsSessionActive)
                 {
                     RunTimers();
                 }
             }).ConfigureAwait(false);


        }
        private void SetEndSession(object sender, EventArgs e)
        {
            if (_session.End != DateTime.MinValue)
            {
                return;
            }
            MySession.End = DateTime.Now;
            StopTimers();
            MySession = MySession;
            App.Db.UpdateSession(MySession);
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            SessionWatch = SessionWatch;
        }

        private void StartExercice(object sender, EventArgs e)
        {
            var exercice = new MyExercicePage(MySession.SessionGuid);
            Navigation.PushAsync(exercice);
        }

        private void OnExerciceSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (ExercicesList.SelectedItem == null) return;
            var selectedMyExercice = e.SelectedItem as MyExerciceModel;
            ExercicesList.SelectedItem = null;
            if (selectedMyExercice == null) return;
            Navigation.PushAsync(new MyExercicePage(MySession.SessionGuid, selectedMyExercice.ExerciseGuid));
        }
    }
}