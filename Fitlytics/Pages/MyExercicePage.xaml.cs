﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Timers;
using Fitlytics.Models;
using Fitlytics.Services;
using Xamarin.Forms.Xaml;
using Timer = System.Timers.Timer;

namespace Fitlytics.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyExercicePage : INotifyPropertyChanged
    {
        private MyExerciceModel _myExercice = new MyExerciceModel();
        private Timer _timer = new Timer();
        private bool _isNewMyExercice;
        private StopWatchWithOffset _watch;

        public new event PropertyChangedEventHandler PropertyChanged;

        public MyExerciceModel MyMyExercice
        {
            get => _myExercice;
            set
            {
                _myExercice = value;
                OnMyPropertyChanged();
            }
        }

        public StopWatchWithOffset MyExerciceWatch
        {
            get => _watch;
            set
            {
                _watch = value;
                OnMyPropertyChanged();
            }
        }

        private void OnMyPropertyChanged([CallerMemberName] string stringProperty = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(stringProperty));
        }

        public MyExercicePage(Guid sessionId, Guid myExerciceId)
        {
            _isNewMyExercice = false;
            MyMyExercice.ExerciseGuid = myExerciceId;
            MyMyExercice.SessionGuid = sessionId;

            InitializeComponent();
        }
        public MyExercicePage(Guid sessionId)
        {
            _isNewMyExercice = true; 
            MyMyExercice.ExerciseGuid = Guid.NewGuid();
            MyMyExercice.SessionGuid = sessionId;
            MyMyExercice.Start = DateTime.Now;

            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            BindingContext = this;
            base.OnAppearing();
            MyExerciceWatch = new StopWatchWithOffset();
            _timer.Interval = TimeSpan.FromSeconds(1).TotalMilliseconds;
            _timer.Elapsed += TimerOnElapsed;
            if (_isNewMyExercice)
            {
                App.Db.AddMyExercice(MyMyExercice);
                RunTimers();
                _isNewMyExercice = false;
            }
            else
            {
                GetMyExercice();
            }
        }

        protected override void OnDisappearing()
        {
            MyMyExercice.TimeElapsed = MyExerciceWatch.ElapsedTimeSpan;
            App.Db.UpdateMyExercice(MyMyExercice);
            base.OnDisappearing();
        }
        private void RunTimers()
        {
            MyExerciceWatch.Start();
            _timer.Start();
        }
        private void StopTimers()
        {
            MyExerciceWatch.Stop();
            _timer.Stop();
        }
        private async void GetMyExercice()
        {

            await Task.Run(() =>
             {
                 MyMyExercice = App.Db.GetMyExercice(myExerciceGuid: MyMyExercice.ExerciseGuid).Result;
                 TimerOnElapsed(null, null);
                 if (MyMyExercice.IsExerciseActive)
                 {
                     RunTimers();
                 }
             }).ConfigureAwait(false);
            MyExerciceWatch.SetElapsedTimeSpan(MyMyExercice.TimeElapsed);
        }

        private void SetEndMyExercice(object sender, EventArgs e)
        {
            if (_myExercice.End != DateTime.MinValue)
            {
                return;
            }
            MyMyExercice.End = DateTime.Now;
            StopTimers();
            MyMyExercice = MyMyExercice;
            App.Db.UpdateMyExercice(MyMyExercice);
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            MyExerciceWatch = MyExerciceWatch;
        }
    }
}