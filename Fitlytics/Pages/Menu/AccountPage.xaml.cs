﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Fitlytics.Pages.Menu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccountPage : ContentPage
    {
        private readonly App _app;

        public AccountPage()
        {
            InitializeComponent();
            _app = Application.Current as App;
            UserName.Text = "Hello " + _app.Name + " !";
        }

        private void DeleteUser(object sender, EventArgs e)
        {
            _app.Name = "";
            _app.IsUserCreated = false;
            DropTable();
            Application.Current.MainPage = new Registration();
            Navigation.PopToRootAsync();
        }

        private void DropTable()
        {
            App.Db.ClearAllTable();
        }
    }
}