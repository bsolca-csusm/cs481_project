﻿using SQLite;

namespace Fitlytics.Persistence
{
    public interface ISqLiteDb
    {
        SQLiteAsyncConnection GetConnection();
    }
}
