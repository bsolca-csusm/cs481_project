﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Fitlytics.Models;
using Fitlytics.Persistence;
using SQLite;
using Xamarin.Forms;

namespace Fitlytics.Services
{
    public class SqLiteDatabase
    {
        private int _secondToWait = 0;
        private readonly SQLiteAsyncConnection _connect;
        private ObservableCollection<SessionModel> _sessions;
        private ObservableCollection<MyExerciceModel> _myExercices;

        public SqLiteDatabase()
        {
            _connect = DependencyService.Get<ISqLiteDb>().GetConnection();
            Connect();
        }

        private async void Connect()
        {
            await _connect.CreateTableAsync<SessionModel>().ConfigureAwait(false);
            await _connect.CreateTableAsync<MyExerciceModel>().ConfigureAwait(false);
        }

        public async void AddSession(SessionModel session)
        {
            Thread.Sleep((int)TimeSpan.FromSeconds(_secondToWait).TotalMilliseconds);
            await _connect.InsertAsync(session).ConfigureAwait(false);
        }

        public async void UpdateSession(SessionModel session)
        {
            Thread.Sleep((int)TimeSpan.FromSeconds(_secondToWait).TotalMilliseconds);
            await _connect.UpdateAsync(session).ConfigureAwait(false);
        }

        public async Task<ObservableCollection<SessionModel>> GetSessions()
        {
            Thread.Sleep((int)TimeSpan.FromSeconds(_secondToWait).TotalMilliseconds);
            var sessions = await _connect.Table<SessionModel>()
                .OrderByDescending(e => e.Start)
                .ToListAsync()
                .ConfigureAwait(false);

            _sessions = new ObservableCollection<SessionModel>(sessions);
            return _sessions;
        }

        public async Task<SessionModel> GetSession(Guid sessionGuid)
        {
            Thread.Sleep((int)TimeSpan.FromSeconds(_secondToWait).TotalMilliseconds);
            var sessions = await _connect.Table<SessionModel>().ToListAsync().ConfigureAwait(false);
            var session = sessions.Find(e => e.SessionGuid.Equals(sessionGuid));
            return await Task.FromResult(session).ConfigureAwait(false);
        }

        public async void AddMyExercice(MyExerciceModel exerice)
        {
            Thread.Sleep((int)TimeSpan.FromSeconds(_secondToWait).TotalMilliseconds);
            await _connect.InsertAsync(exerice).ConfigureAwait(false);
        }

        public async void UpdateMyExercice(MyExerciceModel exerice)
        {
            Thread.Sleep((int)TimeSpan.FromSeconds(_secondToWait).TotalMilliseconds);
            await _connect.UpdateAsync(exerice).ConfigureAwait(false);
        }

        public async Task<ObservableCollection<MyExerciceModel>> GetMyExercices(Guid sessionGuid)
        {
            Thread.Sleep((int)TimeSpan.FromSeconds(_secondToWait).TotalMilliseconds);
            var myExercices = await _connect.Table<MyExerciceModel>()
                .Where(e => e.SessionGuid == sessionGuid)
                .OrderByDescending(e => e.Start)
                .ToListAsync()
                .ConfigureAwait(false);

            _myExercices = new ObservableCollection<MyExerciceModel>(myExercices);
            return _myExercices;
        }

        public async Task<MyExerciceModel> GetMyExercice(Guid myExerciceGuid)
        {
            Thread.Sleep((int)TimeSpan.FromSeconds(_secondToWait).TotalMilliseconds);
            var myExercices = await _connect.Table<MyExerciceModel>().ToListAsync().ConfigureAwait(false);
            var myExercice = myExercices.Find(e => e.ExerciseGuid.Equals(myExerciceGuid));
            return await Task.FromResult(myExercice).ConfigureAwait(false);
        }

        public async void ClearAllTable()
        {
            Thread.Sleep((int)TimeSpan.FromSeconds(_secondToWait).TotalMilliseconds);
            await _connect.DeleteAllAsync<SessionModel>().ConfigureAwait(false);
            await _connect.DeleteAllAsync<MyExerciceModel>().ConfigureAwait(false);
        }
    }
}