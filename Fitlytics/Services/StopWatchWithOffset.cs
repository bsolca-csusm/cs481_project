﻿using System;
using System.Diagnostics;

namespace Fitlytics.Services
{
    public class StopWatchWithOffset
    {
        private readonly Stopwatch _stopwatch;
        private TimeSpan _offsetTimeSpan = TimeSpan.Zero;

        public StopWatchWithOffset()
        {
            _stopwatch = new Stopwatch();
        }
        public void SetElapsedTimeSpan(TimeSpan offsetElapsedTimeSpan)
        {
            _offsetTimeSpan = offsetElapsedTimeSpan;
        }
        public void Start()
        {
            _stopwatch.Start();
        }

        public void Stop()
        {
            _stopwatch.Stop();
        }

        public TimeSpan ElapsedTimeSpan
        {
            get => _stopwatch.Elapsed + _offsetTimeSpan;
            set => _offsetTimeSpan = value;
        }
    }
}