﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Fitlytics.Models;
using Fitlytics.Pages;
using Fitlytics.Pages.Menu;
using Xamarin.Forms;

namespace Fitlytics
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        internal ObservableCollection<SessionModel> SessionsObsCollection;
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            SessionsObsCollection = App.Db.GetSessions().Result;
            SessionsList.ItemsSource = SessionsObsCollection;
            base.OnAppearing();
        }

        private void NavigationAccount(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AccountPage());
        }

        private void StartSession(object sender, EventArgs e)
        {
            SessionPage session = new SessionPage();
            Navigation.PushAsync(session);
        }

        private void NavigationMagicGradient(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MagicGradient());
        }

        private void OnSessionSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (SessionsList.SelectedItem == null) return;
            var selectedSession = e.SelectedItem as SessionModel;
            SessionsList.SelectedItem = null;
            if (selectedSession == null) return;
            Navigation.PushAsync(new SessionPage(selectedSession.SessionGuid));
        }
    }
}
