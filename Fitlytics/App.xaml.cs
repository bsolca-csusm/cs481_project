﻿using Fitlytics.Services;
using Xamarin.Forms;

namespace Fitlytics
{
    public partial class App : Application
    {
        private const string UserNameKey = "Name";
        private const string UserCreatedKey = "UserCreated";
        internal static SqLiteDatabase Db = new SqLiteDatabase();

        public App()
        {
            InitializeComponent();
            var startPage = IsUserCreated ? (Page) new MainPage() : new Registration();
            MainPage = new NavigationPage(startPage);
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        public string Name
        {
            get => Properties.ContainsKey(UserNameKey) ? (string) Properties[UserNameKey] : "";
            set => Properties[UserNameKey] = value;
        }

        public bool IsUserCreated
        {
            get => Properties.ContainsKey(UserCreatedKey) && (bool) Properties[UserCreatedKey];
            set => Properties[UserCreatedKey] = value;
        }
    }
}
