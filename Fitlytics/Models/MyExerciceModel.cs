﻿using System;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace Fitlytics.Models
{
    public class MyExerciceModel
    {
        [PrimaryKey]
        public Guid ExerciseGuid { get; set; }
        [ForeignKey(typeof(SessionModel))]
        public Guid SessionGuid { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public TimeSpan TimeElapsed { get; set; }
        public bool IsExerciseEnd => End != DateTime.MinValue;
        public bool IsExerciseActive => End == DateTime.MinValue;
        public string ExerciseName { get; set; }
        public string ExerciseWeight { get; set; }
        public string ExerciseRepetition { get; set; }
    }
}
