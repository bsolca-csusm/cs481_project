﻿using System;
using SQLite;

namespace Fitlytics.Models
{
    public class SessionModel
    {
        [PrimaryKey]
        public Guid SessionGuid { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public TimeSpan TimeElapsed { get; set; }
        public bool IsSessionEnd => End != DateTime.MinValue;
        public bool IsSessionActive => End == DateTime.MinValue;
    }
}
